﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Common.Mappings.Templates
{
    public class UserMapper : IMapper<Domain.Entities.UserProfile, DistributedServices.Entities.UserProfileDto>
    {
        public Domain.Entities.UserProfile Map(DistributedServices.Entities.UserProfileDto obj)
        {
            if (obj == null)
                return new Domain.Entities.UserProfile();

            return new Domain.Entities.UserProfile
            {
                UserId = obj.UserId,
                Username = obj.Username,
                ClientToken = obj.ClientToken,
				Password = obj.Password
            };
        }

        public DistributedServices.Entities.UserProfileDto Map(Domain.Entities.UserProfile obj)
        {
            if (obj == null)
                return new DistributedServices.Entities.UserProfileDto();

            return new DistributedServices.Entities.UserProfileDto
            {
				UserId = obj.UserId,
				Username = obj.Username,
				ClientToken = obj.ClientToken,
				Password = obj.Password
			};
        }


        public List<Domain.Entities.UserProfile> Map(List<DistributedServices.Entities.UserProfileDto> objs)
        {
            if (objs == null)
                return new List<Domain.Entities.UserProfile>();

            var items = objs.Select(obj => new Domain.Entities.UserProfile
            {
				UserId = obj.UserId,
				Username = obj.Username,
				ClientToken = obj.ClientToken,
				Password = obj.Password
			});

            return items.ToList();
        }

        public List<DistributedServices.Entities.UserProfileDto> Map(List<Domain.Entities.UserProfile> objs)
        {
            if (objs == null)
                return new List<DistributedServices.Entities.UserProfileDto>();

            var items = objs.Select(obj => new DistributedServices.Entities.UserProfileDto
            {
				UserId = obj.UserId,
				Username = obj.Username,
				ClientToken = obj.ClientToken,
				Password = obj.Password
			});

            return items.ToList();
        }
    }
}
