﻿using Domain.Entities;
using Infrastructure.Common.Caching;
using Infrastructure.Data.MainModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.MainModule.Users
{
    public class UserProfileRepository : IUserProfileRepository
    {
        private readonly SecurityContext _context = new SecurityContext();

        public List<Domain.Entities.UserProfile> GetManyBy(Func<Domain.Entities.UserProfile, bool> predicate)
        {
            var items = _context.UserProfiles.Where(predicate);

			var userPasswords = _context.UserPasswords.ToList();

			var usersWithPasswords = new List<Domain.Entities.UserProfile>();

			foreach (var u in items)
			{
				var userPassword = userPasswords.FirstOrDefault(i => i.UserId == u.UserId);

				if (userPassword != null)
					u.Password = userPassword.Password;

				usersWithPasswords.Add(u);
			}

			return items.ToList();
        }

        public Domain.Entities.UserProfile GetBy(Func<Domain.Entities.UserProfile, bool> predicate)
        {
            var item = _context.UserProfiles.FirstOrDefault(predicate);

			var userPassword = _context.UserPasswords.FirstOrDefault(u => u.UserId == item.UserId);

			var userWithPassword = new UserProfile
			{
				UserId = item.UserId,
				Username = item.Username,
				ClientToken = item.ClientToken,
				Password = userPassword == null ? string.Empty : userPassword.Password
			};

            return userWithPassword;
        }

		public Domain.Entities.UserProfile Add(Domain.Entities.UserProfile item)
        {
            var addeditem = _context.UserProfiles.Add(item);

            _context.SaveChanges();

            return addeditem;
        }

        public Domain.Entities.UserProfile Update(Domain.Entities.UserProfile item)
        {
            var itemToUpdate = _context.UserProfiles.FirstOrDefault(i => i.UserId == item.UserId);

            itemToUpdate.Username = item.Username;

            itemToUpdate.ClientToken = item.ClientToken;
            
            _context.SaveChanges();

            return itemToUpdate;
        }

        public Domain.Entities.UserProfile Delete(int id)
        {
            var itemToRemove = _context.UserProfiles.FirstOrDefault(i => i.UserId == id);

            var deletedItem = _context.UserProfiles.Remove(itemToRemove);

            _context.SaveChanges();

            return itemToRemove;
        }

        public List<Domain.Entities.UserProfile> GetAll(string clientToken)
        {
			var guid = Guid.Parse(clientToken);

            var items = _context.UserProfiles.Where(u => u.ClientToken == guid);

			var userPasswords = _context.UserPasswords.ToList();

			var usersWithPasswords = new List<Domain.Entities.UserProfile>();

			foreach (var u in items)
			{
				var userPassword = userPasswords.FirstOrDefault(i => i.UserId == u.UserId);

				if (userPassword != null)
					u.Password =  userPassword.Password;

				usersWithPasswords.Add(u);
			}

			return usersWithPasswords;
        }
    }
}
