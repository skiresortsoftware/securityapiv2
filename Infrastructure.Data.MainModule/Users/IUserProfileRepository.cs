﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Data.MainModule.Users
{
    public interface IUserProfileRepository : IRepository<Domain.Entities.UserProfile>
    {
        List<UserProfile> GetAll(string clientToken);
    }
}
