using Domain.Entities;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace Infrastructure.Data.MainModule.Models
{
    public partial class SecurityContext : DbContext
    {
        static SecurityContext()
        {
            Database.SetInitializer<SecurityContext>(null);
        }

        public SecurityContext()
            : base("Name=SecurityContext")
        {
        }

        public DbSet<Domain.Entities.UserProfile> UserProfiles { get; set; }

		public DbSet<Domain.Entities.UserPassword> UserPasswords { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
			modelBuilder.Entity<UserProfile>().ToTable("dbo.UserProfile");
        }
    }
}
