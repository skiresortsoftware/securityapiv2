﻿using DistributedServices.Entities;
using Infrastructure.Data.MainModule.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Users
{
    public class UserService : IUserService
    {
        private readonly IUserProfileRepository _userRepository;

        public UserService(IUserProfileRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public Domain.Entities.UserProfile GetBy(Func<Domain.Entities.UserProfile, bool> predicate)
        {
            var item = _userRepository.GetBy(predicate);

            return item;
        }

        public Domain.Entities.UserProfile Add(Domain.Entities.UserProfile item)
        {
            var addedItem = _userRepository.Add(item);

            return addedItem;
        }

        public Domain.Entities.UserProfile Update(Domain.Entities.UserProfile item)
        {
            var updatedItem = _userRepository.Update(item);

            return updatedItem;
        }

        public Domain.Entities.UserProfile Delete(int id)
        {
            var deletedItem = _userRepository.Delete(id);

            return deletedItem;
        }

        public List<Domain.Entities.UserProfile> GetManyBy(Func<Domain.Entities.UserProfile, bool> predicate)
        {
            var items = _userRepository.GetManyBy(predicate);

            return items;
        }

        public List<Domain.Entities.UserProfile> GetAll(string clientToken)
        {
            var items = _userRepository.GetAll(clientToken);

            return items;
        }
    }
}
