﻿using DistributedServices.Entities;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Users
{
    public interface IUserService : IService<Domain.Entities.UserProfile>
    {
        List<UserProfile> GetAll(string clientToken);
    }
}
