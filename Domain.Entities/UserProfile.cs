﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class UserProfile
    {
		[Key]
		public int UserId { get; set; }
        public string Username { get; set; }
        public Guid ClientToken { get; set; }

		[NotMapped]
		public string Password { get; set; }
    }
}
