﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
	public class Authentication
	{
		public string Username { get; set; }

		public string NewUsername { get; set; }

		public string Password { get; set; }

		public string NewPassword { get; set; }

		public string ClientToken { get; set; }

		public int UserId { get; set; }
	}

	public class UserPassword
	{
		[Key]
		public int Id { get; set; }

		public string Password { get; set; }

		public int UserId { get; set; }
	}
}
