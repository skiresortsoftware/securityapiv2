﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedServices.Entities
{
    public class UserProfileDto
    {
		public int UserId { get; set; }
		public string Username { get; set; }
		public Guid ClientToken { get; set; }
		public string Password { get; set; }
	}
}
