﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DistributedServices.Api.ViewModels
{
	public class UserInfo
	{
		public int Id { get; set; }
		public Guid ClientToken { get; set; }
		public string Username { get; set; }

		[NotMapped]
		public int PasswordId { get; set; }

		[NotMapped]
		public string Password { get; set; }
	}
}