﻿using DistributedServices.Api.Contexts;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
//using System.Web.Mvc;
using WebMatrix.WebData;

namespace DistributedServices.Api.Controllers
{
	[EnableCors("*", "*", "*")]
	[RoutePrefix("api")]
	public class AuthenticationsController : ApiController
	{
		// private readonly IAuthenticationService _authenticationService;

		private readonly UsersContext _usersContext = new UsersContext();

		public AuthenticationsController()
		{
			//var authenticationRepository = new AuthenticationRepository();

			//_authenticationService = new AuthenticationService(authenticationRepository);
		}

		// GET api/authentications/5
		//public HttpResponseMessage Get([FromUri]Authentication authentication)
		//{
		//	//var getAuthenticationRequest = new Authentication() { Username = authentication.Username, Password = authentication.Password };

		//	//var getAuthenticationResponse = _authenticationService.GetAuthentication(getAuthenticationRequest);

		//	//return Request.CreateResponse(HttpStatusCode.OK, getAuthenticationResponse);

		//	return null;
		//}

		[Route("authentications/{userId}")]
		public HttpResponseMessage Post([FromBody]Authentication authentication, [FromUri]int userId)
		{
			var user = _usersContext.UserProfiles.Find(userId);

			if (user != null)
			{
				if (authentication.NewPassword != null)
				{
					authentication.ClientToken = user.ClientToken.ToString();

					WebSecurity.ChangePassword(user.UserName, authentication.Password, authentication.NewPassword);

					return Request.CreateResponse(HttpStatusCode.OK, authentication);
				}

				//if (authentication.NewUsername != null)
				//{
				//	authentication.ClientToken = authentication.ClientToken.ToString();

				//	user.UserName = authentication.NewUsername;

				//	_usersContext.SaveChanges();

				//	return Request.CreateResponse(HttpStatusCode.OK, authentication);
				//}
				
			}

			return null;
		}

		// POST api/authentications
		[Route("authentications/add")]
		public HttpResponseMessage Get(string username, string password, string clientToken)
		{
			var user = _usersContext.UserProfiles.FirstOrDefault(u => u.UserName == username);

			if (user == null)
			{
				WebSecurity.CreateUserAndAccount(username, password, new { ClientToken = Guid.Parse(clientToken) });

				user = _usersContext.UserProfiles.FirstOrDefault(u => u.UserName == username);

				System.Web.Security.Roles.AddUserToRole(user.UserName, "User");

				var addedUserPassword = AddUserPassword(new UserPassword
				{
					Password = password,
					UserId = user.UserId
				});

				var addedUserEmployeeInfo = new Authentication
				{
					Username = user.UserName,
					UserId = user.UserId,
					Password = password,
					ClientToken = clientToken
				};

				return Request.CreateResponse(HttpStatusCode.OK, addedUserEmployeeInfo);
			}
			else
				return null;
		}

		private UserPassword AddUserPassword(UserPassword userPassword)
		{
			var addedUserPassword = _usersContext.UserPasswords.Add(userPassword);

			_usersContext.SaveChanges();

			return addedUserPassword;
		}

		// PUT api/authentications/5
		public HttpResponseMessage Put([FromBody]Authentication authentication)
		{
			//var updateAuthenticationRequest = new Authentication() { Username = authentication.Username, Password = authentication.Password };

			//var updateAuthenticationResponse = _authenticationService.UpdateAuthentication(updateAuthenticationRequest);

			//return Request.CreateResponse(HttpStatusCode.OK, updateAuthenticationResponse);

			return null;
		}

		// DELETE api/authentications/
		public HttpResponseMessage Delete([FromBody]Authentication authentication)
		{
			//var deleteAuthenticationRequest = new Authentication() { Username = authentication.Username, Password = authentication.Password };

			//var deleteAuthenticationResponse = _authenticationService.DeleteAuthentication(deleteAuthenticationRequest);

			//return Request.CreateResponse(HttpStatusCode.OK, deleteAuthenticationResponse);

			return null;
		}
	}
}