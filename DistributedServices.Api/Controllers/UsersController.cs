﻿using Application.Services.Users;
using DistributedServices.Api.Contexts;
using DistributedServices.Api.Filters;
using DistributedServices.Api.Helpers;
using DistributedServices.Entities;
using Infrastructure.Common.Caching;
using Infrastructure.Common.Mappings;
using Infrastructure.Common.Mappings.Templates;
using Infrastructure.Data.MainModule.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace DistributedServices.Api.Controllers
{
    //[BasicAuthorizationFilter]
    [EnableCors("*", "*", "*")]
    [RoutePrefix("api")]
    public class UsersController : ApiController
    {
        private IUserService _userService;

        private IMapper<Domain.Entities.UserProfile, DistributedServices.Entities.UserProfileDto> _mapper;

        private readonly ICache<DistributedServices.Entities.UserProfileDto> _cache;

        // private const string TemplateCacheIdFormat = "users-{0}";

		public UsersController(IUserService userService, IMapper<Domain.Entities.UserProfile, DistributedServices.Entities.UserProfileDto> mapper)
		{
			_userService = userService;

			_mapper = mapper;

			// _cache = cache;
		}

		[Route("users")]
        public List<Domain.Entities.UserProfile> GetAll(string clientToken)
        {
			var items = _userService.GetAll(clientToken).ToList();

			return items;

        }

        [Route("users/{id}")]
        public Domain.Entities.UserProfile GetBy(int id)
        {
			var item = _userService.GetBy(i => i.UserId == id);

			return item;
        }

        [Route("users")]
        public Response<UserProfileDto> Post(UserProfileDto template, string clientToken)
        {
            var mappedItem = _mapper.Map(template);

            var addedItem = _userService.Add(mappedItem);

            var mappedAddedItem = _mapper.Map(addedItem);

            // _cache.Add(mappedAddedItem, string.Format(TemplateCacheIdFormat, addedItem.UserId));

            return ApiResponse<UserProfileDto>.Success(mappedAddedItem);
        }

        [Route("users/{id}")]
        public Response<UserProfileDto> Put(int id, UserProfileDto item)
        {
            var itemToUpdate = _userService.GetBy(i => i.UserId == id);

            if (itemToUpdate == null)
                return ApiResponse<UserProfileDto>.BadRequest(string.Format("No template with id {0} can be found.", id));

            item.UserId = id;

            var mappedItem = _mapper.Map(item);

            var updatedItem = _userService.Update(mappedItem);

            var mappedUpdatedItem = _mapper.Map(updatedItem);

            // _cache.Update(mappedUpdatedItem, string.Format(TemplateCacheIdFormat, mappedUpdatedItem.UserId));

            return ApiResponse<UserProfileDto>.Success(mappedUpdatedItem);
        }

        [Route("users/{id}")]
        public Response<UserProfileDto> Delete(int id)
        {
            var itemToDelete = _userService.GetBy(i => i.UserId == id);

            if (itemToDelete == null)
                return ApiResponse<UserProfileDto>.BadRequest(string.Format("No template with id {0} can be found.", id));

            var deletedItem = _userService.Delete(id);

            // _cache.Remove(string.Format(TemplateCacheIdFormat, id));

            return ApiResponse<UserProfileDto>.Success(_mapper.Map(deletedItem));
        }
    }
}
